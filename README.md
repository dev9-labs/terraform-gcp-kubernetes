# Overview

1. Start with a Service Account that has project create, IAM, and various other permissions
    1. IAM Role/Needs:

    ```
    resourcemanager.projects.create
    resourcemanager.projects.delete
    resourcemanager.projects.get
    resourcemanager.projects.getIamPolicy
    resourcemanager.projects.list
    resourcemanager.projects.move
    resourcemanager.projects.setIamPolicy
    resourcemanager.projects.update
    ```
1. Add environment variables
    1. abc
    1. def
1. For a client
    1. Setup CI/CD to run terraform project
    1. Optionally create a folder
    1. Create a project
    1. Create the infrastructure
1. abc
