# output "role_id" {
#   value       = "${google_project_iam_custom_role.dev9_service_account.role_id}"
#   description = "Role ID used"
# }
# output "roles" {
#   value       = ["${google_iam_policy.*.role}"]
#   description = "Roles created on account"
# }

output "sa_email" {
  value = "${google_service_account.service_account.email}"
}
