variable "service_account_email" {
  type        = "string"
  description = "Email value of the target service account on the Organization Level"
}

variable "project" {
  type        = "string"
  description = "Project to create Service Account in"
}

variable "roles" {
  type = "list"

  default = [
    "roles/billing.admin",
    "roles/orgpolicy.policyAdmin",
    "roles/resourcemanager.folderAdmin",
    "roles/resourcemanager.folderIamAdmin",
    "roles/resourcemanager.lienModifier",
    "roles/owner",
    "roles/resourcemanager.projectDeleter",
    "roles/resourcemanager.projectCreator",
    "roles/resourcemanager.projectMover",
  ]

  description = "List of roles to bind to service account"
}

variable "org_id" {
  type        = "string"
  description = "Organization to apply to"
}

###############################


# variable "role_id" {
#   type = "string"
# }


# variable "role_name" {
#   type = "string"
# }


# variable "description" {
#   type = "string"
# }


# variable "permissions" {
#   type = "list"


#   default = [
#     "iam.roles.list",
#     "iam.roles.create",
#     "iam.roles.delete",
#   ]
# }


# variable "additional_permissions" {
#   type        = "list"
#   default     = []
#   description = "Additional permissions to be added to role"
# }

