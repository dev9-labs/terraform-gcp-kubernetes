###################################################################################################
###################################################################################################
###################################################################################################

locals {
  required_apis = ["cloudresourcemanager.googleapis.com", "cloudbilling.googleapis.com", "iam.googleapis.com"]
}

data "google_organization" "org" {
  organization = "${var.org_id}"
}

resource "google_project" "project" {
  name            = "${var.project_name}"
  project_id      = "${var.project_name}"
  billing_account = "${var.billing_account}"

  # Guessing these two conflict, folder and ord
  # org_id          = "${var.org_id}"
  folder_id = "${google_folder.parent_folder.name}"
}

resource "google_folder" "parent_folder" {
  display_name = "${var.folder_name}"
  parent       = "${data.google_organization.org.name}"
}

resource "google_project_iam_policy" "policy" {
  count = "${length(var.roles)}"

  project     = "${google_project.project.project_id}"
  policy_data = "${data.google_iam_policy.singleprojectadmin.policy_data}"
}

### /* Grant IAM policies onto folder for user */ ###
# resource "google_project_iam_policy" "policy" {
#   count = "${length(var.roles)}"

#   project     = "${google_project.project.project_id}"
#   policy_data = "${data.google_iam_policy.projectadmin.*.policy_data[count.index]}"
# }

# Enable APIs on project
resource "google_project_services" "project" {
  project = "${google_project.project.project_id}"

  services = "${concat(local.required_apis, var.additional_apis)}"
}

#### Data Providers
data "google_iam_policy" "singleprojectadmin" {
  binding {
    role = "roles/owner"

    members = [
      # NOTE: Future could add ServiceAccount for specalized SA on folder and resources
      "user:${var.owner_email}",
    ]
  }
}

# NOTE DOES NOT WORK YET
data "google_iam_policy" "projectadmin" {
  count = "${length(var.roles)}"

  binding {
    role = "${element(var.roles, count.index)}"

    members = [
      # NOTE: Future could add ServiceAccount for specalized SA on folder and resources
      "user:${var.owner_email}",
    ]
  }
}

# TODO: Setup variable for flag if random IDs should be appended to project or folder IDs/Names
# resource "random_id" "id" {
#   byte_length = 4
#   prefix      = "${var.project_name}-"
# }

